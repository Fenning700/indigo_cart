Basic shopping cart UI that can automatically add products to accommodate existing products in the cart.

Built as part of a job application to Indigo Distribution.


Specifications:

A basic shopping cart User Interface that contains the following products:

- RAK-RAK8-MB
- RAK-WMT-400
- WK-HUB
- RAK-RAK-LINK
       

The conditions of the shopping cart are as follows:

A RAK-RAK8-MB can accommodate up to 8 RAK-WMT-400s, any more and an additional RAK-RAK8-MB will need to be automatically added to the shopping cart.

One RAK-RAK8-MB must be added to the shopping cart for every eight RAK-WMT-400s.

For every shopping cart that contains a RAK-RAK8-MB, one WK-HUB must be automatically added.

One RAK-RAK-LINK must be added to the shopping cart for every four RAK-RAK8-MBs.