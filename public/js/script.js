
var shoppingCart = new Vue({
  el: '.count',
// State products to be counted & autoPopulate toggle 
  data: {

    autoPopulate : [],
    wmtCount : 0,
    rak8Count : 0,
    linkCount : 0,
    hubCount : 0,
    totalCount : 0,

  },

  methods: {
    // Define function to autopopulate the cart
    checkOffer() {
      var self = this;
      // Once more than 8/16/24 etc. WMT-400s are added, another RAK-8 is added
      if ( ((self.rak8Count*8)+1) === self.wmtCount ) {
        self.rak8Count+=1;
        self.totalCount+=1;
      }
      // Once more than 4/8/12 etc. RAK-8s are added, another LINK is added
      if ( ((self.linkCount*4) + 1) === self.rak8Count ) {
        self.linkCount+=1;
        self.totalCount+=1;
        if (self.rak8Count === 1) {
          self.hubCount+=1;
          self.totalCount+=1;
        }
      }
    },

    // Define functions to count added products
    wmtAdd(){
      this.wmtCount +=1;
      this.totalCount +=1;
      // Checkbox toggle for auto populate
      if (this.autoPopulate[0]=="true"){
        this.checkOffer();
      }
    },
    rak8Add(){
      this.rak8Count +=1;
      this.totalCount +=1;
      if (this.autoPopulate[0]=="true"){
        this.checkOffer();
      }
    },
    linkAdd(){
      this.linkCount +=1;
      this.totalCount +=1;
      if (this.autoPopulate[0]=="true"){
        this.checkOffer();
      }
    },
    hubAdd(){
      this.hubCount +=1;
      this.totalCount +=1;
      if (this.autoPopulate[0]=="true"){
        this.checkOffer();
      }
    },
  },

})



